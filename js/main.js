$(document).ready(function() {
	$('.form a').click(function(){
		error = false;
		$('.form input').each(function(e){
			var places = $(this).attr('data-place');
			if(places==$(this).val() || $(this).val()==""){
				$(this).addClass('error');
				error = true;
			}else{
				$(this).removeClass('error');
			}
		});
		if(!error){
			str = $('.form').serialize();
			/*$('#myModal_1').trigger('reveal:close');*/
			$.ajax({
				type: "POST",
				url: "include/form.php?action=callback",
				data: str,
				success: function(msg){
					if(msg=="OK"){
						
						$('.form input').each(function(e){
							var places = $(this).attr('data-place');
							$(this).val(places);
						});
						setTimeout(function() {$('#myModal3').reveal($(this).data());}, 500);
					}
				}
			});
			
		}
		return false;
	});
});
